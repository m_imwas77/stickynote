package stickynote;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class mainMenu {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					mainMenu window = new mainMenu();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public mainMenu() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void setUpFrame() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setVisible(true);
	}
	private void MoveTOAddNewUserPage() {
		new AddNewUser();
		frame.dispose();
	}
	private void MoveTOAddNewNotePage() {
		new AddNewNote();
		frame.dispose();
	}
	private void setUpAddNewUserButton() {
		JButton addNewUserButton = new JButton("Add new user");
		addNewUserButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MoveTOAddNewUserPage();
			}
		});
		addNewUserButton.setBounds(165, 35, 110, 23);
		frame.getContentPane().add(addNewUserButton);	
	}
	private void setUpAddNewNoteButton() {
		JButton addNewNoteButton = new JButton("add new note");
		addNewNoteButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MoveTOAddNewNotePage();
			}
		});
		addNewNoteButton.setBounds(165, 78, 110, 23);
		frame.getContentPane().add(addNewNoteButton);	
	}
	private void setUpViewAllNoteButton() {
		JButton viewAllNoteButton = new JButton("view all note");
		viewAllNoteButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new ViewAllNote();
				frame.getBaselineResizeBehavior();
			}
		});
		viewAllNoteButton.setBounds(165, 124, 110, 23);
		frame.getContentPane().add(viewAllNoteButton);
	}
	private void setUpExitButton() {
		JButton exitButton = new JButton("exit");
		exitButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		exitButton.setBounds(165, 176, 110, 23);
		frame.getContentPane().add(exitButton);
	}
	private void initialize() {
		setUpFrame();
		setUpAddNewUserButton();
		setUpAddNewNoteButton();
		setUpViewAllNoteButton();
		setUpExitButton();
		
	}
}
