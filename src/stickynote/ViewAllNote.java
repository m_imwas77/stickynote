package stickynote;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

public class ViewAllNote {

	private JFrame frame;
	private JTextField userNameTextField;
	private JButton ViewAllNoteButton;
	private JButton BackButton;
	private String usersNamefilePath = System.getProperty("user.home") + "/Desktop/userName.txt";
	private File usersNameFile;
	private HashSet<String> usersName;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ViewAllNote window = new ViewAllNote();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ViewAllNote() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private  void  setUpFrame() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setVisible(true);
	}
	private void setUpUserNameTextField () {
		userNameTextField = new JTextField();
		userNameTextField.setBounds(80, 30, 86, 20);
		frame.getContentPane().add(userNameTextField);
		userNameTextField.setColumns(10);	
	}
	private boolean checkValidUserName() {
		if(usersName.contains(userNameTextField.getText().toString().trim())) {
			return true;
		}
		return false;
	}
	private String getFileContante() {
		String userName = userNameTextField.getText().toString().trim();
		String filePath = System.getProperty("user.home") + "/Desktop/"+userName+".txt";
		String text = "";
		try { 
        	BufferedReader reader = new BufferedReader(new FileReader(filePath));
            while (reader.readLine() != null) {
            	text+=reader.readLine();
            	text+="\n";
            }
            JOptionPane.showMessageDialog(frame, text);
		 } catch (IOException e) {
		    	JOptionPane.showMessageDialog(frame, "An error occurred.");
		 } 
		return "";
	}
	private  void  setUpViewAllNoteButton() {
		ViewAllNoteButton = new JButton("Show");
		ViewAllNoteButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(checkValidUserName()){
					getFileContante();
				}else {
					JOptionPane.showMessageDialog(frame,"user not found");
				}
			}
		});
		ViewAllNoteButton.setBounds(80, 129, 89, 23);
		frame.getContentPane().add(ViewAllNoteButton);
	}
	
	private void setUpBackButton() {
			BackButton = new JButton("Back");
			BackButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					new mainMenu();
					frame.dispose();
				}
			});
			BackButton.setBounds(236, 129, 89, 23);
			frame.getContentPane().add(BackButton);
	}
	private void openFile() {
		usersNameFile = new File(usersNamefilePath);
	    if (usersNameFile.exists()) {
	    	usersName=new HashSet<String>();  
	        try { 
	        	BufferedReader reader = new BufferedReader(new FileReader(usersNamefilePath));
	            while (reader.readLine() != null) {
	            	usersName.add(reader.readLine());
	            }
			 } catch (IOException e) {
			    	JOptionPane.showMessageDialog(frame, "An error occurred.");
			 } 
	    } else {
			JOptionPane.showMessageDialog(frame,"there is a error when try to open user name file ");  
	     }
	}
	private void initialize() {
		setUpFrame();
		setUpUserNameTextField();
		setUpViewAllNoteButton();
		setUpBackButton();
		openFile();
	}
}
