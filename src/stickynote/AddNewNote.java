package stickynote;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JEditorPane;
import javax.swing.JButton;
import javax.swing.JTextPane;

import javafx.scene.control.TextField;
import jdk.internal.org.objectweb.asm.util.CheckAnnotationAdapter;

import java.awt.Canvas;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.HashSet;

import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AddNewNote {

	private JFrame frame;
	private JTextField userNameTextField;
	private JTextArea noteTextArea;
	private JButton addNewNoteButton ;
	private String usersNamefilePath = System.getProperty("user.home") + "/Desktop/userName.txt";
	private File usersNameFile;
	private HashSet<String> usersName;
	private String userNameFilePath = System.getProperty("user.home") + "/Desktop/";
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddNewNote window = new AddNewNote();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AddNewNote() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void setUpFrame() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setVisible(true);
	}
	private void setUpUserNametextField() {
		userNameTextField = new JTextField();
		userNameTextField.setBounds(50, 27, 86, 20);
		frame.getContentPane().add(userNameTextField);
		userNameTextField.setColumns(10);
	}
	private void setUpNoteTextArea() {
		noteTextArea = new JTextArea(30,30);
		noteTextArea.setBounds(171, 31, 180, 201);
		frame.getContentPane().add(noteTextArea);
	}
	private boolean checkValidUserName() {
		if(usersName.contains(userNameTextField.getText().toString().trim())) {
			return true;
		}
		return false;
	}

	private boolean checkNonEmptyNote() {
		if(noteTextArea.getText().toString().length() > 0) {
			return true;
		}
		return false;
	}
	
	private void addNewNote() {
		String userName = userNameTextField.getText().toString().trim();
		try { 
		      FileWriter writer = new FileWriter(userNameFilePath+userName+".txt",true);
		      writer.write(LocalDate.now().toString()+System.getProperty("line.separator"));
		      writer.write(noteTextArea.getText().toString() + System.getProperty("line.separator"));
		      writer.close();
		      JOptionPane.showMessageDialog(frame, "Successfully wrote to the file.");
		} catch (IOException e) {
		    	JOptionPane.showMessageDialog(frame,"An error occurred.");
		}
	}
	private void MoveTOMainMenuBage() {
		new mainMenu();
		frame.dispose();
	}
	private void setUpAddNewNoteButton() {
		addNewNoteButton = new JButton("add new note");
		addNewNoteButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.out.println(usersName);
				if(checkValidUserName()&&
				checkNonEmptyNote()){
					addNewNote();
					MoveTOMainMenuBage();
				}else {
					JOptionPane.showMessageDialog(frame, "Error");
				}
			}

			
		});
		addNewNoteButton.setBounds(26, 142, 110, 23);
		frame.getContentPane().add(addNewNoteButton);
		
	}
	


	private void openFile() {
		usersNameFile = new File(usersNamefilePath);
	    if (usersNameFile.exists()) {
	    	usersName=new HashSet<String>();  
	        try { 
	        	BufferedReader reader = new BufferedReader(new FileReader(usersNamefilePath));
	            while (reader.readLine() != null) {
	            	usersName.add(reader.readLine());
	            }
			 } catch (IOException e) {
			    	JOptionPane.showMessageDialog(frame, "An error occurred.");
			 } 
	    } else {
			JOptionPane.showMessageDialog(frame,"there is a error when try to open user name file ");  
	     }
	}
	private void initialize() {
		
		setUpFrame();
		setUpAddNewNoteButton();
		setUpNoteTextArea();
		setUpUserNametextField();
		openFile();	
	}
}
