package stickynote;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class AddNewUser {

	private JFrame frame;
	private JTextField userNameTextField;
	private JButton addNewUserButton;
	private String usersNamefilePath = System.getProperty("user.home") + "/Desktop/userName.txt";
	private File usersNameFile;
	private HashSet<String> usersName;
	private String userNameFilePath = System.getProperty("user.home") + "/Desktop/";
	private File userNameFile;
	private JButton BackButton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddNewUser window = new AddNewUser();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AddNewUser() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private  void  setUpFrame() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setVisible(true);
	}
	private void setUpUserNameTextField () {
		userNameTextField = new JTextField();
		userNameTextField.setBounds(80, 30, 86, 20);
		frame.getContentPane().add(userNameTextField);
		userNameTextField.setColumns(10);	
	}
	private void MoveTOMainMenuBage() {
		new mainMenu();
		frame.dispose();
	}
	private  void  setUpaddNewUserButton() {
		addNewUserButton = new JButton("add");
		addNewUserButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addNewUser();
				MoveTOMainMenuBage();
			}
		});
		addNewUserButton.setBounds(80, 129, 89, 23);
		frame.getContentPane().add(addNewUserButton);
		{
			BackButton = new JButton("Back");
			BackButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					MoveTOMainMenuBage();
				}
			});
			BackButton.setBounds(236, 129, 89, 23);
			frame.getContentPane().add(BackButton);
		}
	}
	
	private void openFile() {
		usersNameFile = new File(usersNamefilePath);
	    if (usersNameFile.exists()) {
	    	usersName=new HashSet<String>();  
	        try { 
	        	BufferedReader reader = new BufferedReader(new FileReader(usersNamefilePath));
	            while (reader.readLine() != null) {
	            	usersName.add(reader.readLine());
	            }
			 } catch (IOException e) {
			    	JOptionPane.showMessageDialog(frame, "An error occurred.");
			 } 
	    } else {
	    	try {
	    		usersNameFile.createNewFile(); 
			} catch (Exception e) {
				 JOptionPane.showMessageDialog(frame,"there's a error when i try to create file");  
			}
	     }
	}
	private void writToFile(String userName) {
		try { 
		      FileWriter writer = new FileWriter(usersNamefilePath,true);
		      writer.write(userName+System.getProperty("line.separator"));
		      writer.close();
		      usersName.add(userName);
		      System.out.println(usersName);
		      JOptionPane.showMessageDialog(frame, "Successfully wrote to the file.");
		} catch (IOException e) {
		    	JOptionPane.showMessageDialog(frame,"An error occurred.");
		} 
	}
	private void createUserFile(String userName) {
		userNameFilePath += userName+".txt";
		userNameFile = new File(userNameFilePath);
		try {
    		userNameFile.createNewFile();
		} catch (Exception e) {
			 JOptionPane.showMessageDialog(frame,"there's a error when i try to create file");  
		}
	}
	private void addNewUser() {
		String userName = userNameTextField.getText().toString().trim();
		if(usersName.contains(userName)) {
			JOptionPane.showMessageDialog(frame, "name is Already exists");
		}else {
			writToFile(userName);
			createUserFile(userName);
		}
	}
	private void initialize() {
		setUpFrame();
		setUpUserNameTextField();
		setUpaddNewUserButton();
		openFile();
	}

}
